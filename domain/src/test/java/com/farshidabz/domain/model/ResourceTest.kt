package com.farshidabz.domain.model

import org.junit.Test
import com.google.common.truth.Truth.assertThat

class ResourceTest {
    @Test
    fun `give valid resource transform to success it return success data`() {
        val input = Resource.Success("test")
        val output = input.transform { "test" + "test" }
        assertThat((output as Resource.Success).data).isEqualTo("testtest")
    }

    @Test
    fun `give invalid resource transfrom it to error return error data`() {
        val input = Resource.Error("message", "test")
        val output = input.transform { "test" + "test" }
        assertThat((output as Resource.Error).data).isEqualTo("testtest")
    }

    @Test
    fun `give loading resource transform it to loading state return loading`() {
        val input = Resource.Loading("test")
        val output = input.transform { "test" + "test" }
        assertThat((output as Resource.Loading).data).isEqualTo("testtest")
    }

    @Test
    fun `give valid resources get data or null return success data`() {
        val input = Resource.Success("test")
        assertThat(input.getDataOrNull()).isEqualTo("test")
    }

    @Test
    fun `give invalid resources get data or null return error data`() {
        val input = Resource.Error("message", "test")
        assertThat(input.getDataOrNull()).isEqualTo("test")
    }

    @Test
    fun `give loading state get data or null return loading state successfully`() {
        val input = Resource.Loading("test")
        assertThat(input.getDataOrNull()).isEqualTo("test")
    }

}
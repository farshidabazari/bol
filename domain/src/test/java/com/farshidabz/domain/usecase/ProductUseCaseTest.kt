package com.farshidabz.domain.usecase

import com.farshidabz.domain.repository.ProductRepository
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class ProductUseCaseTest {
    private lateinit var useCase: ProductUseCase

    private val mockRepository = Mockito.mock(ProductRepository::class.java)

    @Before
    fun setup() {
        useCase = ProductUseCase(mockRepository)
    }

    @Test
    fun getProduct() {
        runBlocking {
            launch {
                val productId = "productId"
                useCase.getProduct(productId)
                Mockito.verify(mockRepository).getProduct(productId)
                this.cancel()
            }
            return@runBlocking
        }
    }
}
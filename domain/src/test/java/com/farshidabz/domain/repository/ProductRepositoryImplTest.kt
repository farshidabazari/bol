package com.farshidabz.domain.repository

import app.cash.turbine.test
import com.farshidabz.domain.FakeDataUtil
import com.farshidabz.domain.datasource.local.FakeProductPersistenceDataSource
import com.farshidabz.domain.datasource.remote.FakeProductRemoteDataSource
import com.farshidabz.domain.model.Resource
import com.farshidabz.domain.model.getDataOrNull
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class ProductRepositoryImplTest {

    private lateinit var productRepository: ProductRepository
    private lateinit var fakeProductRemoteDataSource: FakeProductRemoteDataSource
    private lateinit var fakeProductPersistenceDataSource: FakeProductPersistenceDataSource

    @Before
    fun setup() {
        fakeProductRemoteDataSource = FakeProductRemoteDataSource()
        fakeProductPersistenceDataSource = FakeProductPersistenceDataSource()

        productRepository = ProductRepositoryImpl(
            fakeProductRemoteDataSource,
            fakeProductPersistenceDataSource
        )
    }

    @Test
    fun `give valid remote response then call get product then return success and update cache`() =
        runBlocking {
            val testRemoteResponse = FakeDataUtil.product
            fakeProductRemoteDataSource.fake_setBehavior(FakeProductRemoteDataSource.Behavior.SHOULD_RETURN_VALID_DATA)
            fakeProductRemoteDataSource.fake_setValidData(testRemoteResponse)

            productRepository.getProduct("id").test {
                var resource = awaitItem()
                while (resource !is Resource.Success) {
                    resource = awaitItem()
                }

                assertThat(resource.getDataOrNull()).isEqualTo(testRemoteResponse)

                cancelAndIgnoreRemainingEvents()
            }
        }

    @Test
    fun `give cached data and invalid remote response then call get product then return success cached data`() =
        runBlocking {
            fakeProductRemoteDataSource.fake_setBehavior(FakeProductRemoteDataSource.Behavior.SHOULD_RETURN_INVALID)

            productRepository.getProduct("id").test {
                val resource = awaitItem()
                assertThat(resource.getDataOrNull()).isEqualTo(null)

                cancelAndIgnoreRemainingEvents()
            }
        }
}
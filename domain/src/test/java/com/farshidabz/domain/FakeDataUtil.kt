package com.farshidabz.domain

import com.farshidabz.domain.model.OfferData
import com.farshidabz.domain.model.Product

object FakeDataUtil {

    val product = Product(
        "id",
        "ean",
        " gpc",
        " title",
        "specsTag",
        4f,
        " shortDescription",
        " longDescription",
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        OfferData(1, 1, 1, emptyList()),
        emptyList()
    )
}

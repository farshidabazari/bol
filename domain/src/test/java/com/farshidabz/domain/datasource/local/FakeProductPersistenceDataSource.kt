package com.farshidabz.domain.datasource.local

import com.farshidabz.domain.FakeDataUtil
import com.farshidabz.domain.model.Product
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.stateIn
import kotlin.coroutines.coroutineContext

class FakeProductPersistenceDataSource : ProductPersistenceDataSource {

    private var storedProduct = FakeDataUtil.product

    override suspend fun isFavorite(id: String) = storedProduct.id == id

    override suspend fun addToFavorite(id: String) {
        TODO("Not yet implemented")
    }

    override suspend fun removeFavorite(id: String) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteAll() {
        TODO("Not yet implemented")
    }

    fun fake_getAll() = storedProduct
}
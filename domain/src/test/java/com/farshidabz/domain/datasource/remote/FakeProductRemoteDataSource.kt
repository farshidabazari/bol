package com.farshidabz.domain.datasource.remote

import com.farshidabz.domain.FakeDataUtil
import com.farshidabz.domain.model.Product
import com.farshidabz.domain.model.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeProductRemoteDataSource : ProductRemoteDataSource {
    private var behavior = Behavior.SHOULD_RETURN_VALID_DATA
    private var validData = FakeDataUtil.product

    override fun getProduct(id: String): Flow<Resource<Product>> = flow {
        emit(Resource.Loading())
        emit(
            when (behavior) {
                Behavior.SHOULD_RETURN_VALID_DATA -> Resource.Success(validData)
                Behavior.SHOULD_RETURN_INVALID -> Resource.Error("No data")
            }
        )
    }

    fun fake_setBehavior(behavior: Behavior) {
        this.behavior = behavior
    }

    fun fake_setValidData(data: Product) {
        this.validData = data
    }

    enum class Behavior {
        SHOULD_RETURN_VALID_DATA,
        SHOULD_RETURN_INVALID,
    }
}
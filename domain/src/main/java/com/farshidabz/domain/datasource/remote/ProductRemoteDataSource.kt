package com.farshidabz.domain.datasource.remote

import com.farshidabz.domain.model.Product
import com.farshidabz.domain.model.Resource
import kotlinx.coroutines.flow.Flow

interface ProductRemoteDataSource {
    fun getProduct(id: String): Flow<Resource<Product>>
}
package com.farshidabz.domain.datasource.local

interface ProductPersistenceDataSource {
    suspend fun isFavorite(id: String): Boolean
    suspend fun addToFavorite(id: String)
    suspend fun removeFavorite(id: String)
    suspend fun deleteAll()
}
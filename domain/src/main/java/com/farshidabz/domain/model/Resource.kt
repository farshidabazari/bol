package com.farshidabz.domain.model

import com.farshidabz.domain.model.Resource.*

/**
 * A generic data wrapper with 3 possible states : [Success], [Error], [Loading]
 */
sealed class Resource<out T> {
    data class Success<T>(val data: T) : Resource<T>()
    data class Error<T>(val message: String, val data: T? = null) : Resource<T>()
    data class Loading<T>(val data: T? = null) : Resource<T>()
}

/**
 * Performs the [transform] action on data and returns a new [Resource] with the same state but new data
 */
fun <T, R> Resource<T>.transform(transform: ((value: T) -> R)): Resource<R> = when (this) {
    is Success -> Success(transform.invoke(data))
    is Error -> Error(message, data?.let { transform.invoke(it) })
    is Loading -> Loading(data?.let { transform.invoke(it) })
}


fun <T> Resource<T>.getDataOrNull() = when (this) {
    is Success -> data
    is Error -> data
    is Loading -> data
}
package com.farshidabz.domain.repository

import com.farshidabz.domain.model.Product
import com.farshidabz.domain.model.Resource
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
    suspend fun getProduct(id: String): Flow<Resource<Product>>
    suspend fun updateFavorite(id: String): Flow<Boolean>
    suspend fun isFavorite(id: String): Flow<Boolean>
}
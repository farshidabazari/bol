package com.farshidabz.domain.repository

import com.farshidabz.domain.datasource.local.ProductPersistenceDataSource
import com.farshidabz.domain.datasource.remote.ProductRemoteDataSource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ProductRepositoryImpl @Inject constructor(
    private val productRemoteDataSource: ProductRemoteDataSource,
    private val productPersistenceDataSource: ProductPersistenceDataSource
) : ProductRepository {
    override suspend fun getProduct(id: String) = productRemoteDataSource.getProduct(id)

    override suspend fun updateFavorite(id: String) = flow {
        when (productPersistenceDataSource.isFavorite(id)) {
            true -> {
                productPersistenceDataSource.removeFavorite(id)
                emit(false)
            }
            else -> {
                productPersistenceDataSource.addToFavorite(id)
                emit(true)
            }
        }
    }

    override suspend fun isFavorite(id: String) = flow {
        emit(productPersistenceDataSource.isFavorite(id))
    }
}
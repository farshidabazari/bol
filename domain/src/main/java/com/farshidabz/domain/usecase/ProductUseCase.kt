package com.farshidabz.domain.usecase

import com.farshidabz.domain.repository.ProductRepository
import javax.inject.Inject

class ProductUseCase @Inject constructor(private val productRepository: ProductRepository) {
    suspend fun getProduct(id: String) = productRepository.getProduct(id)
    suspend fun updateFavorite(productId: String) = productRepository.updateFavorite(productId)
    suspend fun isFavorite(id: String) = productRepository.isFavorite(id)
}
package com.farshidabz.basicmarketplace.navigation

import android.view.View
import androidx.navigation.findNavController
import com.farshidabz.basicmarketplace.models.AttributeGroup
import com.farshidabz.basicmarketplace.presentation.productDetail.ProductDetailFragmentDirections

object ProductDetailNavigation {
    fun navigateToAttributesScreen(view: View, attributes: List<AttributeGroup>?) {
        attributes?.let {
            val action =
                ProductDetailFragmentDirections.actionProductDetailFragmentToProductSpecsFragment(
                    attributes.toTypedArray()
                )
            view.findNavController().navigate(action)
        }
    }
}
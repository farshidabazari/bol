package com.farshidabz.basicmarketplace.presentation.productDetail

import androidx.lifecycle.*
import com.farshidabz.basicmarketplace.models.toGenericRecyclerItems
import com.farshidabz.domain.model.transform
import com.farshidabz.domain.usecase.ProductUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(private val useCase: ProductUseCase) :
    ViewModel() {
    val productId = MutableLiveData<String>()

    val productDetail = productId.switchMap {
        liveData {
            emitSource(useCase.getProduct(it).map { resource ->
                resource.transform { it.toGenericRecyclerItems() }
            }.asLiveData()).also {
                getIsFavorite()
            }
        }
    }

    val isFavorite = MutableLiveData<Boolean>()

    private fun getIsFavorite() {
        viewModelScope.launch {
            useCase.isFavorite(productId.value ?: "").collect {
                isFavorite.value = it
            }
        }
    }

    fun updateFavoriteItem() {
        viewModelScope.launch {
            useCase.updateFavorite(productId.value ?: "").collect {
                isFavorite.value = it
            }
        }
    }
}

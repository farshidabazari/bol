package com.farshidabz.basicmarketplace.presentation.productDetail.spec

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.farshidabz.basicmarketplace.models.AttributeGroup
import com.farshidabz.basicmarketplace.models.toGenericRecyclerItems
import com.farshidabz.basicmarketplace.utils.genericrecyclerview.GenericRecyclerItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProductSpecsViewModel @Inject constructor() : ViewModel() {
    var attributes = mutableListOf<AttributeGroup>()
    set(value) {
        field = value
        attributesRecyclerItem.value = attributes.map { it.toGenericRecyclerItems() }
    }

    val attributesRecyclerItem = MutableLiveData<List<GenericRecyclerItem>>()
}

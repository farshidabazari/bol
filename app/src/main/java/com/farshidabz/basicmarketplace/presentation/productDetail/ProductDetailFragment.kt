package com.farshidabz.basicmarketplace.presentation.productDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.farshidabz.basicmarketplace.databinding.ProductDetailFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    private val viewModel: ProductDetailViewModel by viewModels()
    lateinit var binding: ProductDetailFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProductDetailFragmentBinding.inflate(inflater, container, false)

        setupBinding(binding)
        setupToolbar()

        setProductId()

        return binding.root
    }

    private fun setupToolbar() {
        binding.toolbar.setAddToFavoriteClickListener {
            viewModel.updateFavoriteItem()
        }

        binding.toolbar.setBackClickListener {
            activity?.onBackPressed()
        }
    }

    private fun setProductId() {
        val args: ProductDetailFragmentArgs by navArgs()
        if (viewModel.productId.value.isNullOrEmpty()) {
            viewModel.productId.value = args.productId.toString()
        }
    }

    private fun setupBinding(binding: ProductDetailFragmentBinding) {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
    }
}
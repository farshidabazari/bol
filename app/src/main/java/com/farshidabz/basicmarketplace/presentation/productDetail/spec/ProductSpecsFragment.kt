package com.farshidabz.basicmarketplace.presentation.productDetail.spec

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.farshidabz.basicmarketplace.R
import com.farshidabz.basicmarketplace.databinding.ProductSpecFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductSpecsFragment : Fragment() {

    private val viewModel: ProductSpecsViewModel by viewModels()
    lateinit var binding: ProductSpecFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ProductSpecFragmentBinding.inflate(inflater, container, false)

        setupBinding(binding)
        setAttributes()

        setupToolbar()

        return binding.root
    }

    private fun setupToolbar() {
        binding.toolbar.addToFavoriteImageView.isVisible = false
        binding.toolbar.setBackClickListener {
            activity?.onBackPressed()
        }

        binding.toolbar.titleTextView.text = getString(R.string.specification)
    }

    private fun setupBinding(binding: ProductSpecFragmentBinding) {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
    }

    private fun setAttributes() {
        val attrs: ProductSpecsFragmentArgs by navArgs()
        viewModel.attributes = attrs.attributeGroups.toMutableList()
    }
}
package com.farshidabz.basicmarketplace

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BasicMarketPlaceApp : Application()
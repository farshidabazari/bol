package com.farshidabz.basicmarketplace.utils

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.bumptech.glide.Glide
import com.farshidabz.basicmarketplace.R
import com.farshidabz.basicmarketplace.models.OfferData
import com.farshidabz.domain.model.Resource
import com.google.android.material.snackbar.Snackbar


/**
 * If the resource passed is an instance of class [Resource.Loading], view will be set to visible,
 * otherwise, it will be se to gone.
 */
@BindingAdapter("app:showLoadingOn")
fun showLoadingBasedOnResource(view: View, res: Resource<Any>?) {
    view.isVisible = res is Resource.Loading
}

/**
 * If the resource passed is an instance of class [Resource.Invalid], a snack bar is shown with
 * the error message
 */
@BindingAdapter("app:showErrorOn")
fun showErrorBasedOnResource(view: View, res: Resource<Any>?) {
    if (res is Resource.Error) {
        Snackbar.make(view, res.message, Snackbar.LENGTH_LONG).show()
    }
}

@BindingAdapter("app:imageUrl")
fun loadImage(imageView: ImageView, url: String) {
    with(imageView) {
        Glide.with(context)
            .load(url)
            .centerCrop()
            .into(this)
    }
}

@BindingAdapter("app:price")
fun setPriceTextView(textView: TextView, offerData: OfferData?) {
    textView.text = offerData?.offers?.firstOrNull()?.price.toString()
}

@BindingAdapter("app:rating")
fun setRatingTextView(textView: TextView, rate: Float?) {
    rate?.let {
        textView.text = (rate / 10f).toString()
    } ?: run {
        textView.text = textView.context.getString(R.string.no_rate_text_place_holder)
    }
}

@BindingAdapter("app:set_snap_helper")
fun setSnapHelper(recyclerView: RecyclerView, shouldSet: Boolean) {
    if (shouldSet) {
        val helper: SnapHelper = PagerSnapHelper()
        helper.attachToRecyclerView(recyclerView)
    }
}

@BindingAdapter("app:bound_favorite")
fun setSnapHelper(imageView: ImageView, isFavorite: Boolean) {
    imageView.setImageResource(if (isFavorite) R.drawable.ic_favorite else R.drawable.ic_favorite_border)
}



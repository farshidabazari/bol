package com.farshidabz.basicmarketplace.utils.genericrecyclerview

import androidx.databinding.BindingAdapter
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.farshidabz.basicmarketplace.R
import com.farshidabz.basicmarketplace.models.Attribute
import com.farshidabz.basicmarketplace.models.Medium
import com.farshidabz.basicmarketplace.models.ParentCategoryPath
import com.farshidabz.domain.model.Resource
import com.farshidabz.domain.model.getDataOrNull


@BindingAdapter("app:bound_items")
fun setRecyclerViewResourceItems(
    recyclerView: RecyclerView,
    items: Resource<List<GenericRecyclerItem>>?
) = setupDataBoundListAdapter(recyclerView, items?.getDataOrNull() ?: emptyList())

@BindingAdapter("app:bound_attrs")
fun setRecyclerViewAttrsItems(
    recyclerView: RecyclerView,
    items: List<Attribute>?
) {
    items?.let { attrs ->
        val genericImageItem =
            attrs.map { GenericRecyclerItem(it, R.layout.item_attrs, BR.attr) }
        setupDataBoundListAdapter(recyclerView, genericImageItem)
    }
}

@BindingAdapter("app:bound_item")
fun setRecyclerViewResourceItem(
    recyclerView: RecyclerView,
    item: Resource<GenericRecyclerItem>?
) {
    item?.getDataOrNull()?.let {
        setupDataBoundListAdapter(recyclerView, listOf(it))
    }
}

@BindingAdapter("app:bound_images")
fun setRecyclerViewImagesItem(recyclerView: RecyclerView, item: List<Medium>?) {
    item?.let { media ->
        val genericImageItem =
            media.map { GenericRecyclerItem(it, R.layout.item_product_image, BR.image) }
        setupDataBoundListAdapter(recyclerView, genericImageItem)
    }
}

@BindingAdapter("app:bound_bread_crumbs")
fun setRecyclerViewBreadCrumbItems(recyclerView: RecyclerView, item: List<ParentCategoryPath>?) {
    item?.let { parent ->
        val genericImageItem =
            parent.map {
                GenericRecyclerItem(
                    it.parentCategories?.lastOrNull()?.name ?: "",
                    R.layout.item_bread_crumb_content,
                    BR.categoryName
                )
            }
        setupDataBoundListAdapter(recyclerView, genericImageItem)
    }
}

@BindingAdapter("app:bound_items")
fun setRecyclerViewItems(
    recyclerView: RecyclerView,
    items: List<GenericRecyclerItem>?
) = setupDataBoundListAdapter(recyclerView, items.orEmpty())

private fun setupDataBoundListAdapter(
    recyclerView: RecyclerView,
    items: List<GenericRecyclerItem>
) {
    var adapter = (recyclerView.adapter as? DataBoundListAdapter)
    if (adapter == null) {
        adapter = DataBoundListAdapter()
        recyclerView.adapter = adapter
    }

    adapter.submitList(items)
}
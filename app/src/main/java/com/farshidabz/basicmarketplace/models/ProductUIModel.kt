package com.farshidabz.basicmarketplace.models

import android.os.Parcelable
import android.view.View
import com.farshidabz.basicmarketplace.navigation.ProductDetailNavigation
import kotlinx.parcelize.Parcelize
import java.util.*

data class ProductUIModel(
    val id: String?,
    val ean: String?,
    val gpc: String?,
    val title: String?,
    val specsTag: String?,
    val rating: Float?,
    val shortDescription: String?,
    val longDescription: String?,
    val attributeGroups: List<AttributeGroup>?,
    val entityGroups: List<EntityGroup>?,
    val urls: List<Url>?,
    val images: List<Image>?,
    val media: List<Medium>?,
    val offerData: OfferData?,
    val parentCategoryPaths: List<ParentCategoryPath>?,
) {
    fun onAttrsClicked(view:View) {
        ProductDetailNavigation.navigateToAttributesScreen(view, attributeGroups)
    }
}

@Parcelize
data class Attribute(val label: String?, val value: String?, val key: String?) : Parcelable

@Parcelize
data class AttributeGroup(
    val title: String?,
    val attributes: List<Attribute>?,
) : Parcelable

data class Entity(
    val id: String?,
    val value: String?,
)

data class EntityGroup(
    val title: String?,
    val entities: List<Entity>?,
)

data class Url(
    val key: String?,
    val value: String?,
)

data class Image(
    val type: String?,
    val key: String?,
    val url: String?,
)

data class Medium(
    val type: String?,
    val key: String?,
    val url: String?,
)

data class SellerRating(
    val ratingMethod: String?,
    val sellerRating: String?,
    val productInformationRating: String?,
    val deliveryTimeRating: String?,
    val shippingRating: String?,
    val serviceRating: String?,
)

data class RecentReviewCounts(
    val positiveReviewCount: Int?,
    val neutralReviewCount: Int?,
    val negativeReviewCount: Int?,
    val totalReviewCount: Int?,
)

data class Seller(
    val id: String?,
    val sellerType: String?,
    val displayName: String?,
    val topSeller: Boolean?,
    val logo: String?,
    val sellerRating: SellerRating?,
    val recentReviewCounts: RecentReviewCounts?,
    val sellerInformation: String?,
    val useWarrantyRepairConditions: Boolean?,
    val approvalPercentage: String?,
    val registrationDate: Date?,
)

data class Offer(
    val id: String?,
    val condition: String?,
    val price: Float?,
    val availabilityCode: String?,
    val availabilityDescription: String?,
    val comment: String?,
    val seller: Seller?,
    val bestOffer: Boolean?,
)

data class OfferData(
    val bolCom: Int?,
    val nonProfessionalSellers: Int?,
    val professionalSellers: Int?,
    val offers: List<Offer>?,
)

data class ParentCategory(
    val id: String?,
    val name: String?,
)

data class ParentCategoryPath(val parentCategories: List<ParentCategory>?)
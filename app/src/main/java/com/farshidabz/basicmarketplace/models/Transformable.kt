package com.farshidabz.basicmarketplace.models

interface Transformable {
    fun fromDomainModel(): Any
}
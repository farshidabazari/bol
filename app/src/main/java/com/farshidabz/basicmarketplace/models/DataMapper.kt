package com.farshidabz.basicmarketplace.models

import androidx.databinding.library.baseAdapters.BR
import com.farshidabz.basicmarketplace.R
import com.farshidabz.basicmarketplace.utils.genericrecyclerview.GenericRecyclerItem
import com.farshidabz.domain.model.*
import com.farshidabz.domain.model.Attribute
import com.farshidabz.domain.model.AttributeGroup
import com.farshidabz.domain.model.Entity
import com.farshidabz.domain.model.EntityGroup
import com.farshidabz.domain.model.Image
import com.farshidabz.domain.model.Medium
import com.farshidabz.domain.model.Offer
import com.farshidabz.domain.model.OfferData
import com.farshidabz.domain.model.ParentCategory
import com.farshidabz.domain.model.ParentCategoryPath
import com.farshidabz.domain.model.RecentReviewCounts
import com.farshidabz.domain.model.Seller
import com.farshidabz.domain.model.SellerRating
import com.farshidabz.domain.model.Url

fun Product.toGenericRecyclerItems() = this.fromDomain().toGenericRecyclerItem()

fun com.farshidabz.basicmarketplace.models.AttributeGroup.toGenericRecyclerItems() =
    GenericRecyclerItem(this, R.layout.item_attr_group, BR.attributes)

fun ProductUIModel.toGenericRecyclerItem() =
    GenericRecyclerItem(this, R.layout.item_product_detail, BR.product)

fun Product.fromDomain() =
    ProductUIModel(
        id,
        ean,
        gpc,
        title,
        specsTag,
        rating,
        shortDescription,
        longDescription,
        attributeGroups?.fromDomainModel(),
        entityGroups?.fromDomainModel(),
        urls?.fromDomainModel(),
        images?.fromDomainModel(),
        media?.fromDomainModel(),
        offerData?.fromDomainModel(),
        parentCategoryPaths?.fromDomainModel()
    )

fun AttributeGroup.fromDomainModel() =
    AttributeGroup(this.title, this.attributes?.fromDomainModel())

fun EntityGroup.fromDomainModel() = EntityGroup(title, entities?.fromDomainModel())

@JvmName("fromDomainModelAttributeGroup")
fun List<AttributeGroup>.fromDomainModel() = this.map { it.fromDomainModel() }

@JvmName("fromDomainModelAttribute")
fun List<Attribute>.fromDomainModel() =
    this.map { com.farshidabz.basicmarketplace.models.Attribute(it.label, it.value, it.key) }

@JvmName("fromDomainEntityGroup")
fun List<EntityGroup>.fromDomainModel() = this.map { it?.fromDomainModel() }

@JvmName("fromDomainModelEntity")
fun List<Entity>.fromDomainModel() =
    this.map { com.farshidabz.basicmarketplace.models.Entity(it.id, it.value) }

@JvmName("fromDomainUrl")
fun List<Url>.fromDomainModel() =
    this.map { com.farshidabz.basicmarketplace.models.Url(it.key, it.value) }

@JvmName("fromDomainImage")
fun List<Image>.fromDomainModel() =
    this.map { com.farshidabz.basicmarketplace.models.Image(it.type, it.key, it.url) }

@JvmName("fromDomainMedium")
fun List<Medium>.fromDomainModel() =
    this.map { com.farshidabz.basicmarketplace.models.Medium(it.type, it.key, it.url) }

@JvmName("fromDomainOfferData")
fun OfferData.fromDomainModel() = com.farshidabz.basicmarketplace.models.OfferData(
    bolCom,
    nonProfessionalSellers,
    nonProfessionalSellers,
    offers?.fromDomainModel()
)

fun List<Offer>.fromDomainModel() = this.map {
    Offer(
        it.id,
        it.condition,
        it.price,
        it.availabilityCode,
        it.availabilityDescription,
        it.comment,
        it.seller?.fromDomainModel(),
        it.bestOffer
    )
}

private fun Seller.fromDomainModel() = Seller(
    id,
    sellerType,
    displayName,
    topSeller,
    logo,
    sellerRating?.fromDomainModel(),
    recentReviewCounts?.fromDomainModel(),
    sellerInformation,
    useWarrantyRepairConditions,
    approvalPercentage,
    registrationDate
)

private fun RecentReviewCounts.fromDomainModel() =
    com.farshidabz.basicmarketplace.models.RecentReviewCounts(
        positiveReviewCount,
        neutralReviewCount,
        negativeReviewCount,
        totalReviewCount
    )

fun SellerRating.fromDomainModel() = com.farshidabz.basicmarketplace.models.SellerRating(
    ratingMethod,
    sellerRating,
    productInformationRating,
    deliveryTimeRating,
    shippingRating,
    serviceRating
)

@JvmName("fromDomainParentCategoryPath")
fun List<ParentCategoryPath>.fromDomainModel() =
    this.map { ParentCategoryPath(it.parentCategories?.fromDomainModel()) }

@JvmName("fromDomainModelParentCategory")
fun List<ParentCategory>.fromDomainModel() =
    this.map { com.farshidabz.basicmarketplace.models.ParentCategory(it.id, it.name) }


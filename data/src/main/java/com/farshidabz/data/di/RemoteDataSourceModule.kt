package com.farshidabz.data.di

import com.farshidabz.data.remote.api.ProductClient
import com.farshidabz.data.remote.datasource.ProductRemoteDataSourceImpl
import com.farshidabz.domain.datasource.remote.ProductRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object RemoteDataSourceModule {
    @Provides
    @ActivityRetainedScoped
    fun provideProductRemoteDataSource(apiClient: ProductClient): ProductRemoteDataSource {
        return ProductRemoteDataSourceImpl(apiClient)
    }
}
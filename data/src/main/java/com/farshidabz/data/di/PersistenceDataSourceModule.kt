package com.farshidabz.data.di

import com.farshidabz.data.local.BasicMarketPlaceDatabase
import com.farshidabz.data.local.dao.FavoriteProductDao
import com.farshidabz.data.local.datasource.ProductPersistenceDataSourceImpl
import com.farshidabz.domain.datasource.local.ProductPersistenceDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object PersistenceDataSourceModule {
    @Provides
    @ActivityRetainedScoped
    fun provideProductDao(db: BasicMarketPlaceDatabase): FavoriteProductDao {
        return db.productDao()
    }

    @Provides
    @ActivityRetainedScoped
    fun provideProductPersistenceDataSource(favoriteProductDao: FavoriteProductDao): ProductPersistenceDataSource {
        return ProductPersistenceDataSourceImpl(favoriteProductDao)
    }
}
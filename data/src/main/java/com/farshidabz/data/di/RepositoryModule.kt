package com.farshidabz.data.di

import com.farshidabz.domain.repository.ProductRepositoryImpl
import com.farshidabz.domain.datasource.local.ProductPersistenceDataSource
import com.farshidabz.domain.datasource.remote.ProductRemoteDataSource
import com.farshidabz.domain.repository.ProductRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object RepositoryModule {
    @Provides
    @ActivityRetainedScoped
    fun provideProductRepository(
        productRemoteDataSource: ProductRemoteDataSource,
        productPersistenceDataSource: ProductPersistenceDataSource
    ): ProductRepository {
        return ProductRepositoryImpl(productRemoteDataSource, productPersistenceDataSource)
    }
}
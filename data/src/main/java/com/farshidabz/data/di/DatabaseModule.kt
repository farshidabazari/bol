package com.farshidabz.data.di

import android.content.Context
import com.farshidabz.data.local.BasicMarketPlaceDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context): BasicMarketPlaceDatabase {
        return BasicMarketPlaceDatabase.getDatabase(appContext)
    }
}
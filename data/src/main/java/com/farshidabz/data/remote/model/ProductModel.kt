package com.farshidabz.data.remote.model

import com.farshidabz.data.mapper.toDomainModel
import com.farshidabz.data.util.Transformable
import com.google.gson.annotations.SerializedName
import java.util.*


data class ProductModel(@SerializedName("products") val products: List<Product>)

data class Product(
    @SerializedName("id")
    val id: String?,
    @SerializedName("ean")
    val ean: String?,
    @SerializedName("gpc")
    val gpc: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("specsTag")
    val specsTag: String?,
    @SerializedName("rating")
    val rating: Float?,
    @SerializedName("shortDescription")
    val shortDescription: String?,
    @SerializedName("longDescription")
    val longDescription: String?,
    @SerializedName("attributeGroups")
    val attributeGroups: List<AttributeGroup>?,
    @SerializedName("entityGroups")
    val entityGroups: List<EntityGroup>?,
    @SerializedName("urls")
    val urls: List<Url>?,
    @SerializedName("images")
    val images: List<Image>?,
    @SerializedName("media")
    val media: List<Medium>?,
    @SerializedName("offerData")
    val offerData: OfferData?,
    @SerializedName("parentCategoryPaths")
    val parentCategoryPaths: List<ParentCategoryPath>?,
)

data class Attribute(
    @SerializedName("label")
    val label: String?,
    @SerializedName("value")
    val value: String?,
    @SerializedName("key")
    val key: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Attribute(label, value, key)
}

data class AttributeGroup(
    @SerializedName("title")
    val title: String?,
    @SerializedName("attributes")
    val attributes: List<Attribute>?,
) : Transformable {
    override fun toDomainModel() =
        com.farshidabz.domain.model.AttributeGroup(
            title,
            attributes?.toDomainModel() as? List<com.farshidabz.domain.model.Attribute>
        )
}

data class Entity(
    @SerializedName("id")
    val id: String?,
    @SerializedName("value")
    val value: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Entity(id, value)
}

data class EntityGroup(
    @SerializedName("title")
    val title: String?,
    @SerializedName("entities")
    val entities: List<Entity>?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.EntityGroup(
        title,
        entities?.toDomainModel() as? List<com.farshidabz.domain.model.Entity>
    )
}

data class Url(
    @SerializedName("key")
    val key: String?,
    @SerializedName("value")
    val value: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Url(key, value)
}

data class Image(
    @SerializedName("type")
    val type: String?,
    @SerializedName("key")
    val key: String?,
    @SerializedName("url")
    val url: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Image(type, key, url)
}

data class Medium(
    @SerializedName("type")
    val type: String?,
    @SerializedName("key")
    val key: String?,
    @SerializedName("url")
    val url: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Medium(type, key, url)
}

data class SellerRating(
    @SerializedName("ratingMethod")
    val ratingMethod: String?,
    @SerializedName("sellerRating")
    val sellerRating: String?,
    @SerializedName("productInformationRating")
    val productInformationRating: String?,
    @SerializedName("deliveryTimeRating")
    val deliveryTimeRating: String?,
    @SerializedName("shippingRating")
    val shippingRating: String?,
    @SerializedName("serviceRating")
    val serviceRating: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.SellerRating(
        ratingMethod,
        sellerRating,
        productInformationRating,
        deliveryTimeRating,
        shippingRating,
        serviceRating
    )
}

data class RecentReviewCounts(
    @SerializedName("positiveReviewCount")
    val positiveReviewCount: Int?,
    @SerializedName("neutralReviewCount")
    val neutralReviewCount: Int?,
    @SerializedName("negativeReviewCount")
    val negativeReviewCount: Int?,
    @SerializedName("totalReviewCount")
    val totalReviewCount: Int?
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.RecentReviewCounts(
        positiveReviewCount,
        neutralReviewCount,
        negativeReviewCount,
        totalReviewCount
    )
}

data class Seller(
    @SerializedName("id")
    val id: String?,
    @SerializedName("sellerType")
    val sellerType: String?,
    @SerializedName("displayName")
    val displayName: String?,
    @SerializedName("topSeller")
    val topSeller: Boolean?,
    @SerializedName("logo")
    val logo: String?,
    @SerializedName("sellerRating")
    val sellerRating: SellerRating?,
    @SerializedName("recentReviewCounts")
    val recentReviewCounts: RecentReviewCounts?,
    @SerializedName("sellerInformation")
    val sellerInformation: String?,
    @SerializedName("useWarrantyRepairConditions")
    val useWarrantyRepairConditions: Boolean?,
    @SerializedName("approvalPercentage")
    val approvalPercentage: String?,
    @SerializedName("registrationDate")
    val registrationDate: Date?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Seller(
        id,
        sellerType,
        displayName,
        topSeller,
        logo,
        sellerRating?.toDomainModel(),
        recentReviewCounts?.toDomainModel(),
        sellerInformation,
        useWarrantyRepairConditions,
        approvalPercentage,
        registrationDate
    )
}

data class Offer(
    @SerializedName("id")
    val id: String?,
    @SerializedName("condition")
    val condition: String?,
    @SerializedName("price")
    val price: Float?,
    @SerializedName("availabilityCode")
    val availabilityCode: String?,
    @SerializedName("availabilityDescription")
    val availabilityDescription: String?,
    @SerializedName("comment")
    val comment: String?,
    @SerializedName("seller")
    val seller: Seller?,
    @SerializedName("bestOffer")
    val bestOffer: Boolean?
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.Offer(
        id,
        condition,
        price,
        availabilityCode,
        availabilityDescription,
        comment,/**/
        seller?.toDomainModel(),
        bestOffer
    )
}

data class OfferData(
    @SerializedName("bolCom")
    val bolCom: Int?,
    @SerializedName("nonProfessionalSellers")
    val nonProfessionalSellers: Int?,
    @SerializedName("professionalSellers")
    val professionalSellers: Int?,
    @SerializedName("offers")
    val offers: List<Offer>?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.OfferData(
        bolCom,
        nonProfessionalSellers,
        nonProfessionalSellers,
        offers?.toDomainModel() as? List<com.farshidabz.domain.model.Offer>
    )
}

data class ParentCategory(
    @SerializedName("id")
    val id: String?,
    @SerializedName("name")
    val name: String?,
) : Transformable {
    override fun toDomainModel() = com.farshidabz.domain.model.ParentCategory(id, name)
}

data class ParentCategoryPath(
    @SerializedName("parentCategories")
    val parentCategories: List<ParentCategory>?,
) : Transformable {
    override fun toDomainModel() =
        com.farshidabz.domain.model.ParentCategoryPath(
            parentCategories?.toDomainModel() as? List<com.farshidabz.domain.model.ParentCategory>
        )
}
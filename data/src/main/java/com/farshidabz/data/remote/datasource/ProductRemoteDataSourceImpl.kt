package com.farshidabz.data.remote.datasource

import com.farshidabz.data.BuildConfig
import com.farshidabz.data.mapper.toDomain
import com.farshidabz.data.remote.api.ProductClient
import com.farshidabz.domain.datasource.remote.ProductRemoteDataSource
import com.farshidabz.domain.model.transform
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ProductRemoteDataSourceImpl @Inject constructor(private val client: ProductClient) :
    ProductRemoteDataSource,
    BaseRemoteDataSource() {

    override fun getProduct(id: String) = safeApiCall {
        client.getProduct(id, BuildConfig.API_KEY)
    }.map { response ->
        response.transform { it.toDomain() }
    }
}
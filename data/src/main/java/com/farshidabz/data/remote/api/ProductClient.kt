package com.farshidabz.data.remote.api

import com.farshidabz.data.BuildConfig
import com.farshidabz.data.remote.model.ProductModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductClient {
    @GET(BuildConfig.PRODUCT_DETAIL_URL)
    suspend fun getProduct(
        @Path("id") id: String,
        @Query("apikey") apiKey: String,
        @Query("format") string: String = "json"
    ): Response<ProductModel>
}
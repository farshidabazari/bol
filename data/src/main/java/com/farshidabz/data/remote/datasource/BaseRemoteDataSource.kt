package com.farshidabz.data.remote.datasource

import com.farshidabz.data.util.NetworkUtils.getErrorMessage
import com.farshidabz.data.util.NetworkUtils.getNetworkErrorMessage
import com.farshidabz.domain.model.Resource
import kotlinx.coroutines.flow.flow
import retrofit2.Response

abstract class BaseRemoteDataSource {
    /**
     * This method  will safely invoke the remote api call and return a flow of [Resource]s
     */
    fun <T : Any> safeApiCall(call: suspend () -> Response<T>) = flow {
        emit(Resource.Loading())
        try {
            emit(safeApiResult(call.invoke()))
        } catch (e: Exception) {
            emit(Resource.Error<T>(getNetworkErrorMessage(e)))
        }
    }

    private fun <T> safeApiResult(response: Response<T>): Resource<T> =
        if (response.isSuccessful && response.body() != null) Resource.Success(response.body()!!)
        else Resource.Error(getErrorMessage(response.code()))
}
package com.farshidabz.data.mapper

import com.farshidabz.data.remote.model.ProductModel
import com.farshidabz.data.util.Transformable
import com.farshidabz.domain.model.*

fun <T : Transformable> List<T>.toDomainModel(): List<Any> {
    return this.map { it.toDomainModel() }
}

fun ProductModel.toDomain() = with(this.products.first()) {
    Product(
        id,
        ean,
        gpc,
        title,
        specsTag,
        rating,
        shortDescription,
        longDescription,
        attributeGroups?.toDomainModel() as? List<AttributeGroup>,
        entityGroups?.toDomainModel() as? List<EntityGroup>,
        urls?.toDomainModel() as? List<Url>,
        images?.toDomainModel() as? List<Image>,
        media?.toDomainModel() as? List<Medium>,
        offerData?.toDomainModel(),
        parentCategoryPaths?.toDomainModel() as? List<ParentCategoryPath>
    )
}
package com.farshidabz.data.util

interface Transformable {
    fun toDomainModel(): Any
}
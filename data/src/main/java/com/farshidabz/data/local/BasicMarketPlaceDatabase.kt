package com.farshidabz.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.farshidabz.data.local.dao.FavoriteProductDao
import com.farshidabz.data.local.model.FavoriteProductEntity

private const val DB_NAME = "products"

@Database(entities = [FavoriteProductEntity::class], version = 1)
abstract class BasicMarketPlaceDatabase : RoomDatabase() {
    abstract fun productDao(): FavoriteProductDao

    companion object {
        @Volatile
        private var instance: BasicMarketPlaceDatabase? = null

        fun getDatabase(context: Context): BasicMarketPlaceDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, BasicMarketPlaceDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}

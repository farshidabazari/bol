package com.farshidabz.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farshidabz.data.local.model.FavoriteProductEntity

@Dao
interface FavoriteProductDao {
    @Query("SELECT * FROM favorite_table")
    suspend fun getAll(): List<FavoriteProductEntity>

    @Query("SELECT EXISTS(SELECT * FROM favorite_table WHERE id = :productId)")
    suspend fun isFavorite(productId: String): Boolean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(favoriteProduct: FavoriteProductEntity)

    @Query("DELETE FROM favorite_table WHERE id = :productId")
    suspend fun delete(productId: String)

    @Query("DELETE FROM favorite_table")
    suspend fun deleteAll()
}
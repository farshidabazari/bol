package com.farshidabz.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_table")
data class FavoriteProductEntity(@PrimaryKey(autoGenerate = false) val id: String)

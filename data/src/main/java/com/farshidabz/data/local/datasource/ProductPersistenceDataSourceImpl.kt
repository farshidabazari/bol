package com.farshidabz.data.local.datasource

import com.farshidabz.data.local.dao.FavoriteProductDao
import com.farshidabz.data.local.model.FavoriteProductEntity
import com.farshidabz.domain.datasource.local.ProductPersistenceDataSource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ProductPersistenceDataSourceImpl @Inject constructor(private val favoriteProductDao: FavoriteProductDao) :
    ProductPersistenceDataSource {
    override suspend fun isFavorite(id: String) = favoriteProductDao.isFavorite(id)

    override suspend fun addToFavorite(id: String) {
        favoriteProductDao.insert(FavoriteProductEntity(id))
    }

    override suspend fun removeFavorite(id: String) {
        favoriteProductDao.delete(id)
    }

    override suspend fun deleteAll() {
        favoriteProductDao.deleteAll()
    }
}
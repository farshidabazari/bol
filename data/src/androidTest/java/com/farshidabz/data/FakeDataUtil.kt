package com.farshidabz.data

import com.farshidabz.data.local.model.FavoriteProductEntity

object FakeDataUtil {

    object local {
        fun getFakeFavoriteProduct() = FavoriteProductEntity("1")

        fun getListOfFakeFavoriteProduct() =
            listOf(FavoriteProductEntity("1"), FavoriteProductEntity("2"))
    }

    object Remote {
        /**
         * This returns a valid response from server
         */
        fun getValidJsonResponse(): String {
            val fileInputStream = javaClass.classLoader?.getResourceAsStream("json/validData.json")
            return fileInputStream?.bufferedReader()?.readText().toString()
        }
    }
}
package com.farshidabz.data.remote.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.farshidabz.data.BaseTest
import com.farshidabz.data.remote.api.ProductClient
import com.farshidabz.domain.datasource.remote.ProductRemoteDataSource
import com.farshidabz.domain.model.Resource
import com.farshidabz.domain.model.getDataOrNull
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
internal class ProductRemoteDataSourceImplTest : BaseTest() {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var productRemoteDataSource: ProductRemoteDataSource

    @Before
    override fun setup() {
        super.setup()
        productRemoteDataSource =
            ProductRemoteDataSourceImpl(getApiService(ProductClient::class.java))
    }

    @Test
    fun giveFakeProductId_getProductDetail_returnSuccess() = runBlocking {
        productRemoteDataSource.getProduct("productId").test(timeoutMs = 60_000L) {
            awaitItem().let {
                Truth.assertThat(it).isInstanceOf(Resource.Loading::class.java)
            }

            awaitItem().let {
                Truth.assertThat(it).isInstanceOf(Resource.Success::class.java)
                Truth.assertThat(it.getDataOrNull()).isNotNull()
            }

            cancelAndIgnoreRemainingEvents()
        }
    }
}
package com.farshidabz.data.remote.utils

import com.farshidabz.data.FakeDataUtil.Remote.getValidJsonResponse
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

/**
 * Routes the request based on the path
 */
internal class RequestDispatcher : Dispatcher() {
    override fun dispatch(request: RecordedRequest): MockResponse {
        return when (request.path) {
            MockNetworkConfig.productDetailFakeUrl -> {
                MockResponse().setResponseCode(MockNetworkConfig.status)
                    .setBody(getValidJsonResponse())
            }

            else -> throw IllegalArgumentException("Unable to find ${request.path}")
        }
    }
}

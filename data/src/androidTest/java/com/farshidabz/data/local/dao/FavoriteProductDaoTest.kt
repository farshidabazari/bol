package com.farshidabz.data.local.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.farshidabz.data.FakeDataUtil
import com.farshidabz.data.local.BasicMarketPlaceDatabase
import com.farshidabz.data.local.DBHelper
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class FavoriteProductDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var db: BasicMarketPlaceDatabase
    private lateinit var favoriteProductDao: FavoriteProductDao

    @Before
    fun setup() {
        db = DBHelper.getInMemoryDb()
        favoriteProductDao = db.productDao()
    }

    @Test
    fun giveProduct_insertToFavoriteAndGet_returnIsFavoriteAsTrue() = runBlocking {
        val inputProduct = FakeDataUtil.local.getFakeFavoriteProduct()
        with(favoriteProductDao) {
            insert(inputProduct)
            Truth.assertThat(isFavorite("1")).isEqualTo(true)
        }
    }

    @Test
    fun giveProductId_insertAndGetAll_returnAllFavoriteProducts() = runBlockingTest {
        val inputProduct = FakeDataUtil.local.getListOfFakeFavoriteProduct()
        with(favoriteProductDao) {

            inputProduct.forEach {
                insert(it)
            }

            Truth.assertThat(getAll()).hasSize(inputProduct.size)
        }
    }

    @Test
    fun deleteAll() = runBlockingTest {
        val inputData = FakeDataUtil.local.getFakeFavoriteProduct()

        with(favoriteProductDao) {
            insert(inputData)
            deleteAll()

            Truth.assertThat(getAll()).isEmpty()
        }
    }

    @After
    fun teardown() {
        db.close()
    }
}
package com.farshidabz.data.local

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider

object DBHelper {
    fun getInMemoryDb() = Room.inMemoryDatabaseBuilder(
        ApplicationProvider.getApplicationContext(),
        BasicMarketPlaceDatabase::class.java
    ).allowMainThreadQueries()
        .build()
}

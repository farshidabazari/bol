package com.farshidabz.data.local


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.farshidabz.data.FakeDataUtil
import com.farshidabz.data.local.dao.FavoriteProductDao
import com.farshidabz.data.local.datasource.ProductPersistenceDataSourceImpl
import com.farshidabz.domain.datasource.local.ProductPersistenceDataSource
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class ProductPersistenceDataSourceImplTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var productPersistenceDataSource: ProductPersistenceDataSource
    private lateinit var db: BasicMarketPlaceDatabase
    private lateinit var favoriteProductDao: FavoriteProductDao

    @Before
    fun setup() {
        db = DBHelper.getInMemoryDb()
        favoriteProductDao = db.productDao()
        favoriteProductDao = db.productDao()
        productPersistenceDataSource = ProductPersistenceDataSourceImpl(favoriteProductDao)
    }

    @Test
    fun isFavorite() = runBlocking {
        val inputData = FakeDataUtil.local.getFakeFavoriteProduct()
        favoriteProductDao.insert(inputData)
        val job = launch {
            val outputProducts = productPersistenceDataSource.isFavorite(inputData.id)
            assertThat(outputProducts).isEqualTo(true)
        }
    }

    @Test
    fun addToFavorite() = runBlocking {
        val inputData = FakeDataUtil.local.getFakeFavoriteProduct()
        favoriteProductDao.insert(inputData)
        val outputProducts = productPersistenceDataSource.isFavorite(inputData.id)
        assertThat(outputProducts).isEqualTo(true)
    }

    @Test
    fun removeFavorite() = runBlocking {
        val inputData = FakeDataUtil.local.getFakeFavoriteProduct()

        favoriteProductDao.insert(inputData)
        favoriteProductDao.delete(inputData.id)

        val outputProducts = productPersistenceDataSource.isFavorite(inputData.id)
        assertThat(outputProducts).isEqualTo(false)
    }

    @After
    fun teardown() {
        db.close()
    }
}